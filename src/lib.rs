use rhai::Dynamic;
use rhai::Engine;
use rhai::Module;
use rhai::Scope;
use rhai::AST;
use serde::{Deserialize, Serialize};
use std::io::{BufRead, BufReader, Read};

use anyhow::{anyhow, Result};

use regex::Regex;

#[derive(Debug, Deserialize, Serialize, PartialEq, Eq)]
pub enum Trigger {
    Regex(String),
    All,
    Begin,
    End,
}

#[derive(Debug)]
pub enum ProcessedTrigger {
    Regex(Regex),
    Begin,
    End,
}

impl Trigger {
    fn process(self) -> Result<ProcessedTrigger> {
        match self {
            Trigger::All => Ok(ProcessedTrigger::Regex(Regex::new("^.*$")?)),
            Trigger::Begin => Ok(ProcessedTrigger::Begin),
            Trigger::End => Ok(ProcessedTrigger::End),
            Trigger::Regex(line) => Ok(ProcessedTrigger::Regex(Regex::new(&line)?)),
        }
    }
}

#[derive(Debug)]
enum LineInfo<'a> {
    Line(&'a str),
    Begin,
    End,
}

impl ProcessedTrigger {
    fn matches<'a>(&self, line: &'a LineInfo<'a>) -> (bool, Option<Vec<Vec<&'a str>>>) {
        match &self {
            &ProcessedTrigger::Begin => {
                if let LineInfo::Begin = line {
                    (true, None)
                } else {
                    (false, None)
                }
            }

            &ProcessedTrigger::End => {
                if let LineInfo::End = line {
                    (true, None)
                } else {
                    (false, None)
                }
            }

            &ProcessedTrigger::Regex(re) => {
                if let LineInfo::Line(line) = line {
                    let mut total_matches: Vec<Vec<&'a str>> = Vec::new();
                    for captures in re.captures_iter(line) {
                        let matches: Vec<&'a str> = captures
                            .iter()
                            .filter_map(|v| v)
                            .map(|v| v.as_str())
                            .collect();
                        total_matches.push(matches);
                    }

                    if total_matches.len() > 0 {
                        (true, Some(total_matches))
                    } else {
                        (false, None)
                    }
                } else {
                    (false, None)
                }
            }
        }
    }
}

impl Default for Trigger {
    fn default() -> Self {
        Trigger::All
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Rule {
    trigger: Trigger,
    body: String,
}

impl Default for Rule {
    fn default() -> Self {
        Rule {
            trigger: Trigger::All,
            body: "print(_line)".to_string(),
        }
    }
}

impl Rule {
    fn process(self, engine: &Engine) -> Result<ProcessedRule> {
        let trigger = self.trigger.process()?;

        let ast = engine.compile(&self.body)?;

        Ok(ProcessedRule { trigger, ast })
    }
}

impl From<(String, String)> for Rule {
    fn from(pair: (String, String)) -> Self {
        let (trigger_str, body) = pair;

        let trigger = if trigger_str.starts_with("/") && trigger_str.ends_with("/") {
            Trigger::Regex(trigger_str[1..trigger_str.len() - 1].to_owned())
        } else {
            match trigger_str.as_str() {
                "" => Trigger::All,
                "BEGIN" => Trigger::Begin,
                "END" => Trigger::End,
                _ => panic!(),
            }
        };

        Rule { trigger, body }
    }
}

#[derive(Debug)]
pub struct ProcessedRule {
    trigger: ProcessedTrigger,
    ast: AST,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Rules {
    preload: Option<String>,
    rules: Vec<Rule>,
}

impl Default for Rules {
    fn default() -> Self {
        Rules {
            preload: None,
            rules: vec![Rule::default()],
        }
    }
}

impl Rules {

    pub fn new_from_rules(rules: Vec<Rule>) -> Rules {
        Rules {
            preload: None,
            rules,
        }
    }

    pub fn process(self) -> Result<ProcessedRules> {
        let engine = Engine::new();

        let mut rules: Vec<ProcessedRule> = Vec::new();

        for rule in self.rules {
            let processed_rule = rule.process(&engine)?;
            rules.push(processed_rule);
        }

        Ok(ProcessedRules {
            engine,
            rules,
            preload: self.preload,
        })
    }
}

pub struct ProcessedRules {
    engine: Engine,
    rules: Vec<ProcessedRule>,
    preload: Option<String>,
}

impl ProcessedRules {
    fn apply_line(
        &self,
        line_counter: &u64,
        line_info: &LineInfo,
        scope: &mut Scope,
    ) -> Result<()> {
        for rule in &self.rules {
            let (found_matches, matches_list) = rule.trigger.matches(&line_info);

            if !found_matches {
                continue;
            }

            scope.set_value("_line_number", *line_counter);

            if let LineInfo::Line(line) = line_info {
                scope.set_value("_line", line.to_string());
            } else {
                scope.set_value("_line", ());
            };

            if let Some(matches_list) = matches_list {
                scope.set_value("_total_matches", matches_list.len() as u64);

                for (index, matches) in matches_list.iter().enumerate() {
                    let owned_matches: Vec<String> =
                        matches.iter().map(|v| v.to_string()).collect();
                    let array_matches: Dynamic = owned_matches.into();

                    scope.set_value("_match_index", (index + 1) as u64);
                    scope.set_value("_matches", array_matches);
                    scope.set_value("_num_matches", matches.len() as u64);

                    let _result: Dynamic =
                        self.engine.eval_ast_with_scope(scope, &rule.ast).unwrap();
                }
            } else {
                scope.set_value("_total_matches", ());
                scope.set_value("_match_index", ());
                scope.set_value("_matches", ());
                scope.set_value("_num_matches", ());

                let _result: Dynamic = self.engine.eval_ast_with_scope(scope, &rule.ast).unwrap();
            }
        }

        Ok(())
    }

    pub fn apply<R: Read>(&self, text: R) -> Result<()> {
        let input = BufReader::new(text);

        let mut scope = Scope::new();

        if let Some(preload) = &self.preload {
            let ast = self.engine.compile(&preload)?;
            let module = Module::eval_ast_as_new(Scope::new(), &ast, &self.engine)
                .map_err(|_| anyhow!("error loading module"))?;
            scope.push_module("pre", module);
        }

        let mut line_info = LineInfo::Begin;

        let mut line_counter = 0;

        self.apply_line(&line_counter, &line_info, &mut scope)?;

        for line in input.lines() {
            let line = line?;
            line_counter += 1;
            line_info = LineInfo::Line(&line);
            self.apply_line(&line_counter, &line_info, &mut scope)?;
        }
        line_info = LineInfo::End;

        self.apply_line(&line_counter, &line_info, &mut scope)?;

        Ok(())
    }
}

#[cfg(test)]
mod test {

    use super::*;

    use serde_yaml;

    use anyhow::Context;

    use std::io::Cursor;

    #[test]
    fn test_serialize_rule() {
        let rule = Rule {
            trigger: Trigger::Regex(".*".to_string()),
            body: "dosth".to_owned(),
        };

        let rules = Rules {
            preload: None,
            rules: vec![rule],
        };

        let serialized = serde_yaml::to_string(&rules).unwrap();
        println!("generated yaml:");
        println!("{}", serialized);
    }

    #[test]
    fn test_deserialize_rules() {
        let yaml_desc = r#"
        rules:
          - trigger:
              Regex: ".*"
            body: bla
          - trigger: Begin
            body: |
              line 1
              line 2
              line 3
        "#;

        let rules: Rules = serde_yaml::from_str(yaml_desc).unwrap();

        println!("{:#?}", &rules);
    }

    #[test]
    fn test_simple_rule() {
        let trigger = "BEGIN".to_string();
        let body = "test".to_string();

        let rule: Rule = (trigger, body).into();

        assert_eq!(rule.trigger, Trigger::Begin);

        let trigger = "/re/".to_string();
        let body = "test".to_string();

        let rule: Rule = (trigger, body).into();

        assert_eq!(rule.trigger, Trigger::Regex("re".to_string()));
    }

    #[test]
    fn test_match() {
        let text = "testtttt";
        let line = LineInfo::Line(&text);

        let trigger = Trigger::Regex(".*".to_string()).process().unwrap();
        let result = trigger.matches(&line);
        println!("{:#?}", result);

        let trigger = Trigger::Regex("(.)(.+)(.)".to_string()).process().unwrap();
        let result = trigger.matches(&line);
        println!("{:#?}", result);

        let trigger = Trigger::Regex("(..)".to_string()).process().unwrap();
        let result = trigger.matches(&line);
        println!("{:#?}", result);

        let trigger = Trigger::All.process().unwrap();
        let result = trigger.matches(&line);
        println!("{:#?}", result);
    }

    #[test]
    fn test_rules() {
        let yaml_desc = r#"
        preload: |
          fn test(x) {
              print("hello from test " + x)
          }
        rules:
          - trigger: Begin
            body: |
              let a = 0;
              pre::test(a);

          - trigger: 
              Regex: ".*"
            body: |
              print(_line);
              print(a);
              a += 1;

          - trigger:
              Regex: "(.)"
            body: >
              print("char: " + _matches[0] + " " + _match_index + "/" + _num_total_matches)

          - trigger: End
            body: |
              print("number of lines: " + _line_number);
              pre::test("a")
        "#;

        let text = r#"a
ab
abc
"#;

        let rules: Rules = serde_yaml::from_str(yaml_desc)
            .context("yaml parsing")
            .unwrap();

        let rules = rules.process().unwrap();

        rules.apply(Cursor::new(text)).unwrap();
    }
}
