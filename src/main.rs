
use std::io::Read;
use std::path::PathBuf;
use std::fs::read_to_string;
use std::fs::File;
use structopt::StructOpt;

use anyhow::{Context, Result};

use serde_yaml;

use rawk;
use rawk::{Rule, Rules};

#[derive(StructOpt, Debug)]
#[structopt(name = "rsed")]
struct Opt {

    #[structopt[short = "f", long = "rules", parse(from_os_str)]]
    rules_file: Option<PathBuf>,

    #[structopt(short = "r", long = "rule")]
    rules: Option<Vec<String>>,

    #[structopt[parse(from_os_str)]]
    input: Option<PathBuf>,
}

fn main() -> Result<()> {

    let opt = Opt::from_args();

    let raw_rules = if let Some(rules_file) = &opt.rules_file {
        let rules_str = read_to_string(&rules_file).context("rules file")?;
        let raw_rules: Rules = serde_yaml::from_str(&rules_str)?;
        raw_rules
    } else if let Some(rules) = &opt.rules {
        let mut parsed_rules = Vec::new();

        for rule in rules {
            let mut rule_def = rule.splitn(2, "::").map(|v| v.to_owned());
            let trigger_str = rule_def.next().unwrap();
            let body_str = rule_def.next().unwrap();

            let rule_def = (trigger_str, body_str);
            let rule: Rule = rule_def.into();
        
            parsed_rules.push(rule);
        }

        Rules::new_from_rules(parsed_rules)
    } else {
        Rules::default()
    };

    let input_file: Box<dyn Read> = if let Some(path) = opt.input {
        Box::new(File::open(path)?)
    } else {
        Box::new(std::io::stdin())
    };

    let rules = raw_rules.process()?;

    rules.apply(input_file)?;

    Ok(())
}
